import 'bootstrap';
import Vue from 'vue'
import App from './App.vue'
import Navbar from "./navbar/navbar.vue"
import Content from "./content/content.vue"
import Footer from "./footer/footer.vue"
import Card from "./card/card.vue"
import Lista from "./lista/lista.vue"
// import Vuikit from 'vuikit'
// import VuikitIcons from '@vuikit/icons'

// import '@vuikit/theme'

// Vue.use(Vuikit)
// Vue.use(VuikitIcons)
Vue.component(Navbar.name,Navbar);
Vue.component(Footer.name,Footer);
Vue.component(Content.name,Content);
Vue.component(Card.name,Card);
Vue.component(Lista.name,Lista);
new Vue({
  el: '#app',
  render: h => h(App)
})
